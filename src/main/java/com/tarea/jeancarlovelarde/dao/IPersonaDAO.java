package com.tarea.jeancarlovelarde.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tarea.jeancarlovelarde.model.Persona;

public interface IPersonaDAO  extends JpaRepository<Persona, Integer>{

}
