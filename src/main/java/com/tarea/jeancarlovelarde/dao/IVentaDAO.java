package com.tarea.jeancarlovelarde.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tarea.jeancarlovelarde.model.Venta;

public interface IVentaDAO extends JpaRepository<Venta, Integer>{

}
