package com.tarea.jeancarlovelarde.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tarea.jeancarlovelarde.model.Producto;

public interface IProductoDAO extends JpaRepository<Producto, Integer>{

}
