package com.tarea.jeancarlovelarde.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.tarea.jeancarlovelarde.model.Persona;
import com.tarea.jeancarlovelarde.model.Producto;
import com.tarea.jeancarlovelarde.service.IProductoService;

@RestController
@RequestMapping("/productos")
public class ProductoController {

	@Autowired
	private IProductoService service;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> listar(){
		List<Producto> productos = new ArrayList<>();
		productos = service.listar();
		return new ResponseEntity<List<Producto>>(productos, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Producto> listarId(@PathVariable("id") Integer id){
		Producto per = service.listarId(id);
		return new ResponseEntity<Producto>(per,HttpStatus.OK);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@RequestBody Producto productos){
		Producto pro = new Producto();
		pro = service.registrar(productos);
		URI url = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pro.getIdProducto()).toUri();
		return ResponseEntity.created(url).build();
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> modificar(@RequestBody Producto pro) {
		Producto producto = service.listarId(pro.getIdProducto());
		String res = "";
		if(producto == null) {
			res = "No hay registro con ese id.";
		}else {		
			res = "Se modifico el registro";
			service.modificar(pro);
		}
		return new ResponseEntity<String>(res,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> eliminar(@PathVariable("id") Integer id) {
		Producto pro = service.listarId(id);
		String res = "";
		if(pro == null) {
			res = "No hay registro con ese id.";
		}else {
			res = "Se elemino el registro";
			service.eliminar(id);
		}
		return new ResponseEntity<String>(res,HttpStatus.OK);
	}
}
