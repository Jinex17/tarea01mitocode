package com.tarea.jeancarlovelarde.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.tarea.jeancarlovelarde.model.Persona;
import com.tarea.jeancarlovelarde.service.IPersonaService;

@RestController
@RequestMapping("/personas")
public class PersonaController {

	@Autowired
	private IPersonaService service;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Persona>> listar(){
		List<Persona> personas = new ArrayList<>();
		personas = service.listar();
		return new ResponseEntity<List<Persona>>(personas, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Persona> listarId(@PathVariable("id") Integer id){
		Persona per = service.listarId(id);
		return new ResponseEntity<Persona>(per,HttpStatus.OK);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@RequestBody Persona persona){
		Persona per = new Persona();
		per = service.registrar(persona);
		URI url = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(per.getIdPersona()).toUri();
		return ResponseEntity.created(url).build();
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> modificar(@RequestBody Persona per) {
		Persona persona = service.listarId(per.getIdPersona());
		String res = "";
		if(persona == null) {
			res = "No hay registro con ese id.";
		}else {		
			res = "Se modifico el registro";
			service.modificar(per);
		}
		return new ResponseEntity<String>(res,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> eliminar(@PathVariable("id") Integer id) {
		Persona per = service.listarId(id);
		String res = "";
		if(per == null) {
			res = "No hay registro con ese id.";
		}else {
			res = "Se elemino el registro";
			service.eliminar(id);
		}
		return new ResponseEntity<String>(res,HttpStatus.OK);
	}
}
