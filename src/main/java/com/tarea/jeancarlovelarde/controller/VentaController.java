package com.tarea.jeancarlovelarde.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.tarea.jeancarlovelarde.model.Producto;
import com.tarea.jeancarlovelarde.model.Venta;
import com.tarea.jeancarlovelarde.service.IVentaService;

@RestController
@RequestMapping("/ventas")
public class VentaController {

	@Autowired
	private IVentaService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Venta>> listar() {
		List<Venta> ventas = new ArrayList<>();
		ventas = service.listar();
		return new ResponseEntity<List<Venta>>(ventas, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Venta> listarId(@PathVariable("id") Integer id) {
		Venta ven = service.listarId(id);
		return new ResponseEntity<Venta>(ven, HttpStatus.OK);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@RequestBody Venta venta) {
		Venta ven = new Venta();
		venta.getDetalleVenta().forEach(x -> x.setIdVenta(venta));
		ven = service.registrar(venta);
		URI url = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(ven.getIdVenta())
				.toUri();
		return ResponseEntity.created(url).build();
	}
}
