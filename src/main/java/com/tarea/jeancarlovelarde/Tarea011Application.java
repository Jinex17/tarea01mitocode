package com.tarea.jeancarlovelarde;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tarea011Application {

	public static void main(String[] args) {
		SpringApplication.run(Tarea011Application.class, args);
	}
}
