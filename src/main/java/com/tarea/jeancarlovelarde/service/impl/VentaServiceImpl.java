package com.tarea.jeancarlovelarde.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarea.jeancarlovelarde.dao.IVentaDAO;
import com.tarea.jeancarlovelarde.model.Venta;
import com.tarea.jeancarlovelarde.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService{

	@Autowired
	private IVentaDAO dao;
	
	@Transactional
	@Override
	public Venta registrar(Venta t) {
		return dao.save(t);
	}

	@Override
	public Venta modificar(Venta t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
	}

	@Override
	public Venta listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Venta> listar() {
		return dao.findAll();
	}

}
